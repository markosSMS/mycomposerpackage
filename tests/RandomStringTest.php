<?php namespace tests;

use PHPUnit\Framework\TestCase;

use Mastojadin\MyComposerPackage\GeneralHelper;

final class RandomStringTest extends TestCase {
    public function test_get_random_string_returns_string() :void
    {
        $this->assertIsString(GeneralHelper::get_random_string());
    }

    public function test_get_random_string_under_half_second_1000() :void
    {
        $time_start = microtime(true);

        for ($i = 0; $i < 1000; $i += 1) {
            $string = GeneralHelper::get_random_string();
        }

        $time_end = microtime(true);

        $time_diff = $time_end - $time_start;

        $this->assertLessThan(0.5, $time_diff);
    }

    public function test_get_random_string_diff_values()
    {
        $strings = [];

        for ($i = 0; $i < 1000; $i += 1) {
            $strings[$i] = GeneralHelper::get_random_string();
        }

        $this->assertEquals(array_unique($strings), $strings);
    }
}
